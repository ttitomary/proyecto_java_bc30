package com.Bank.PassiveTransaction;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@OpenAPIDefinition(info = @Info(title = "passive-transaction-service", version = "0.0.1"))
@SpringBootApplication
public class PassiveTransactionApplication {

	public static void main(String[] args) {
		SpringApplication.run(PassiveTransactionApplication.class, args);
	}

}
